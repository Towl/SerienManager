'''
Created on 06.01.2018

@author: Thorsten
'''
# coding: utf8


from bs4 import BeautifulSoup
import requests
import os
import datetime
import re
from PIL import Image
from string import ascii_lowercase
import traceback

from PyQt5.QtWidgets import QFileDialog, QApplication

class html_Handler(object):
    '''
    classdocs
    '''


    def __init__(self):
        '''
        Constructor
        '''
        
        
        
    def getInformation(self,key, allSeriesDict):
        
        print "Function get Information"
        if key != '':
            
            try:
                if key in allSeriesDict.keys():
                    url_search = allSeriesDict[key]
            except:# old variant to get correct link if not in allSeriesDict
                key_parts = key.lower().split(' ')
                url_search = 'https://www.serienjunkies.de/'
                if len(key_parts) > 1:
                    new_key = key.lower().replace(' ','-')
                else:
                    new_key = key.lower()
                    
                url_search = url_search + new_key + '/alle-serien-staffeln.html'
            
            url_search_episodenguide = url_search
            url_search_main = url_search[:url_search.rfind('/')]
            
            # Get Images of current working directory
            bilder = [x for x in os.listdir('./bilder//') if x.endswith(".jpg")]
            cwd = os.getcwd()
            
            newDict_html = {}
            try:
                main_page = requests.get(url_search_main)
                episods_page = requests.get(url_search_episodenguide)
                
                soup = BeautifulSoup(main_page.content, 'html.parser')
                soup_episodguide = BeautifulSoup(episods_page.content, 'html.parser')
                       
                seriebox = soup.find_all('div', {"class": "topabstand box"})
                try:
                    title = str(soup.find('div', {"class": "fl"}).contents[0].contents[0].upper())
                except:
                    try:
                        title = seriebox[0].contents[0].text
                        title = title[18:title.__len__()-1]
                        #title = str(seriebox[0].contents[0].find_all('a')[0].get('href').replace('/', '').replace('-',' ').upper())
                    except:
                        title = ''
                
                # get Status Beendet/Eingestellt -> True-False       
                
                try:
                    #beendet = seriebox[0].contents[(seriebox[0].__len__())-1].find_all('em')[0].text
                    length =  int(seriebox[0].contents[0].find_all('tr').__len__())
                    beendet = seriebox[0].contents[0].find_all('tr')[length-2].text
                    
                    if (beendet.find('beendet') != -1) or (beendet.find('abgesetzt') != -1) or (beendet.find('eingestellt') != -1):
                        beendet = 'true'
                    else:
                        beendet = 'false'
                except:
                    beendet = 'false'
                
                
               
                
                # Get Image of Serie from web
                serien_bild = soup.find('div', {"id": "serienbild"})
                
                
                serien_bild_url = 'https:' + str(serien_bild.contents[0].find('img')['data-src'])
                # Name of Image
                image_name = os.path.split(serien_bild_url)[1].lower()
             
                # Save Image to Folder if it not exist
                if image_name not in bilder:
                    print "Image was saved ***{}***".format(image_name)
                    r2 = requests.get(serien_bild_url)
                    image_name_url = cwd + '\\bilder\\' + image_name
                    with open(image_name_url, "wb") as f:
                        f.write(r2.content)
                    
                
                # Get Anzahl aller Staffeln -> wird zu Staffel_angek wenn nachfolgende ueberpruefung true
                
                for item in seriebox[0].contents[0].find_all('tr'):
                    item_str = str(item)
                    #print item_str
                    
                    if item_str.find('<td>Anzahl der bestellten Staffeln:</td>') != -1:
                        Staffel_veroeff = int(item.contents[1].text)
                        #print Staffel_veroeff
                
                    
                    # get next Episode date/name
                    if item_str.find('geplante Episodenausstrahlung') != -1:
                        
                        try:
                            nextEpisode_name = item.contents[1].contents[0].text
                            #print nextEpisode_name
                        except:
                            nextEpisode_name = item.contents[1].contents[0]
                            #print nextEpisode_name
                        nextEpisode_date = item.contents[1].contents[1].text 
                        
                        #print nextEpisode_name
                        #print nextEpisode_date
                        
                        DateTime_today = datetime.datetime.now()

                        
                        
                        
                        if nextEpisode_date.lower().find('morgen') != -1:
                            nextEpisode_date = DateTime_today + datetime.timedelta(days=1)
                            
                        elif nextEpisode_date.lower().find('heute') != -1:   
                            nextEpisode_date = DateTime_today
                              
                        else: 
                            daysTillNextEpisode = re.findall("\d+", nextEpisode_date)
                            daysTillNextEpisode = int(daysTillNextEpisode[0])
                            if daysTillNextEpisode != 0:
                                nextEpisode_date = DateTime_today + datetime.timedelta(days=daysTillNextEpisode)
                            else:
                                nextEpisode_date = DateTime_today
                            
                        NextEpisode_date_full = '{} - *{}.{}.{}*'.format(nextEpisode_name,
                                                       nextEpisode_date.day,
                                                       nextEpisode_date.month,
                                                       nextEpisode_date.year)
                    
                        #print NextEpisode_date_full

                # Es Existiert kein Datum fuer die naechste Episode
                try:           
                    if NextEpisode_date_full != 0:
                        pass
                    else:
                        if beendet == 'true':
                            NextEpisode_date_full = ''
                        else:
                            NextEpisode_date_full = 'Next Episode: tbd'
                except:
                    if beendet == 'true':
                        NextEpisode_date_full = ''
                    else:
                        NextEpisode_date_full = 'Next Episode: tbd'
                    
                
                
                # Get Anzahl der Episoden je Staffel 
                allEpisods = soup_episodguide.find_all('div', {"class": "box pad10"})    
                ac = allEpisods[0].find_all('td', {"class": "ac"})
                
                newDict_html[key] = {"Staffeln_veroeff": str(Staffel_veroeff),
                                       "Staffeln_angek": str(Staffel_veroeff),
                                       "Beendet_Eingestellt": str(beendet),
                                       "Next_Episode": str(NextEpisode_date_full)}
                
                i=1
                for episoden in ac:
                    #print "Staffel: {} -> Episoden: {}".format(str(i),str(episoden.text))
                    myStaffel_html_str = "Staffel " + str(i)
                    episoden_ges = str(episoden.text)
                    newDict_html[key].update({myStaffel_html_str : episoden_ges})
                    i += 1
                
                # Wenn die Anzahl der veroeffentlichen Episoden zur jeweiligen Staffel
                # kleiner ist als die Anzahl aller veroeffentlichen Staffeln
                # so gilt Staffel_angek = Staffel_veroeff
                # und    Staffel_veroeff = i-1
                # Steht hier sonst muesste es doppelt geschrieben werden
    
                if int(ac.__len__()) < int(Staffel_veroeff):
                    #print ac.__len__()
                    Staffel_angek = Staffel_veroeff
                    Staffel_veroeff = int(ac.__len__())
                    newDict_html[key].update({"Staffeln_veroeff": str(Staffel_veroeff),
                                            "Staffeln_angek": str(Staffel_angek),
                                            })
                # Update NextEpisode if no new Staffel announced    
                else:
                    if NextEpisode_date_full == 'Next Episode: tbd':  
                        NextEpisode_date_full = ''
                        newDict_html[key].update({"Next_Episode": str(NextEpisode_date_full)})
                
                
                # add ImageName to Dict
                newDict_html[key].update({"IMG": str(image_name)})
                
                
                return newDict_html
            
            except:
                emptyDict = {}
                return emptyDict
        else:
            emptyDict = {}
            
            return emptyDict
    
    
    def getAllSeries(self):
        series_dict = {}
        url = ['https://www.serienjunkies.de/serien/buchstaben-09.html']
        serienjunkies_url = 'https://www.serienjunkies.de'
        alle_serien_ende_url = 'alle-serien-staffeln.html'
        
        for c in ascii_lowercase:
            new_url = serienjunkies_url + '/serien/' + 'buchstaben-' +  c + '.html' 
            url.append(new_url)
        

        for u in url:
            print u 

            page = requests.get(u)
            
            soup = BeautifulSoup(page.content, 'html.parser')
            
                    
            all_series = soup.find_all('div', {"class": "pad10"})[0]
            x = all_series.contents[1].find_all('a', {"class": "b0 grau"})
            
            for s in x:
                try:
                    #print s
                    href = str(s.get('href')) # absolute link
                    rel_link = serienjunkies_url + href + alle_serien_ende_url # relative link
                    # fehler bei codierung muss noch behoben werden
                    #rel_link.decode('unicode-escape') 
                    #rel_link = rel_link.replace('a','ae')
                    #rel_link = rel_link.replace('a','oe')
                    #rel_link = rel_link.replace('a','ue')
                     
                    key = str(s.text)
                    #key.decode('unicode-escape')  
                    #key = key.replace('a','ae')
                    #key = key.replace('a','oe')
                    #key = key.replace('a','ue')
                    
                    print key
                    print rel_link
                    
                    series_dict.update( { key : rel_link } ) # link dictonary
                except:
                    print "except"
    
                    
                    
                    
                    

        savefile = 1
        if savefile:
            cwd = os.getcwd()
            try:
                myfile = open(cwd +"\\" + "seriesDict.txt",'w+')
                for series, link in series_dict.items():
                    myfile.write(series + ";" + link + "\n")
                
                myfile.close()
            except:
                pass
        print "AllSeries saved sucessfully"

        return series_dict


    def writeInformation(self,mySeriesDict):

        html_view_tab = '<td> <span style="display:inline-block; width: 2em;"></span> </td>'
        html_style_text_left = 'style="text-align: left;"' 
        html_table_style ='<table style="float:left; margin-right:50px;">'
        html_SeriesOverview = '' 

        cwd = os.getcwd()
        myHTMLfile = open(cwd +"\\" +"SerienManager.html",'w+')
       

        html_header = """    
        <!doctype html>
        <html lang="de">
        <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Serien Manager</title>
        </head>
        <body>
        <h1>SerienManager</h1>
        <p>Hier werden alle Serien aufgelistet</p>
        <hr />
        """

        #print html_header
        myHTMLfile.write(html_header)
        
    
        html_overview_table = '''
        <table>
            <thead>
                <tr>
                <th style="text-align: left;">Name</th>
                <th>Language</th>
                <th>Season released</th>
                <th>Season announced</th>
                <th>Status</th>
                <th>Finished</th>
                </tr>
            </thead>
            <tbody>
                <tr>
        '''
        #print html_overview_table
        # Loop over all Series
        # write to html table

        
        try:
            for key in mySeriesDict.keys():
                Sprache = mySeriesDict[key]["Sprache"]
                Staffel_veroeff = mySeriesDict[key]["Staffeln_veroeff"]
                Staffeln_angek = mySeriesDict[key]["Staffeln_angek"]
                Beendet_Eingestellt = mySeriesDict[key]["Beendet_Eingestellt"]
                Status = "aktive"
                IMG = mySeriesDict[key]["IMG"]
                NextEpisode = mySeriesDict[key]["Next_Episode"]

                AlleGesehen = 0 # Get Status
                Staffel_veroeff = mySeriesDict[key]["Staffeln_veroeff"]
                
                for Staffel_idx in range(int(Staffel_veroeff)):
                    Staffel_key = "Staffel " + str(Staffel_idx+1)
                    
                    Folgen_gesehen = int(mySeriesDict[key][Staffel_key][0])
                    Folgen_ges = int(mySeriesDict[key][Staffel_key][1])

                    
                    if Folgen_gesehen == Folgen_ges:
                        AlleGesehen += 1
                            
                    if AlleGesehen == int(Staffel_veroeff):
                    
                        Status = "Done"
                    else:                    
                        Status = "Aktive"
                
                html_overview_table += '<td><a href="#' + key + '">' + key + '</a></td>'
                html_overview_table += '<td style="text-align: center;">' + Sprache + '</td>' # Sprache
                html_overview_table += '<td style="text-align: center;">' + Staffel_veroeff + '</td>' # Staffel veroeffentlicht
                html_overview_table += '<td style="text-align: center;">' + Staffeln_angek + '</td>' # Staffel angekuendigt
                html_overview_table += '<td style="text-align: center;">' + Status + '</td>' # Staffel Status (alle gesehen/ja/nein)
                html_overview_table += '<td style="text-align: center;">' + Beendet_Eingestellt + '</td>' # Serie Eingestellt
                
                html_overview_table += '</tr>'

                new_path = cwd + '\\bilder\\' + IMG

                html_SeriesOverview += '<h2 id="' + key + '"><span style="text-decoration: underline;">' + key + '</span></h2>'
                html_SeriesOverview += '<p><img src=' + new_path + ' alt=' + IMG + ' width="800" height="217" /></p>'
                html_SeriesOverview += '<p style="text-decoration: underline;">Overview</p>'
                html_SeriesOverview += '<table>'
                html_SeriesOverview += '<tbody>'
                
                html_SeriesOverview += '<tr>'
                html_SeriesOverview += '<th ' + html_style_text_left + '>' + 'Language:</th>'
                html_SeriesOverview += html_view_tab
                html_SeriesOverview += '<td>' + Sprache + '</td>'
                html_SeriesOverview += '</tr>'

                html_SeriesOverview += '<tr>'
                html_SeriesOverview += '<th ' + html_style_text_left + '>' + 'Season released:</th>'
                html_SeriesOverview += html_view_tab
                html_SeriesOverview += '<td>' + Staffel_veroeff + '</td>'
                html_SeriesOverview += '</tr>'

                html_SeriesOverview += '<tr>'
                html_SeriesOverview += '<th ' + html_style_text_left + '>' + 'Season announced:</th>'
                html_SeriesOverview += html_view_tab
                html_SeriesOverview += '<td>' + Staffeln_angek + '</td>'
                html_SeriesOverview += '</tr>'
                
                html_SeriesOverview += '<tr>'
                html_SeriesOverview += '<th ' + html_style_text_left + '>' + 'Status:</th>'
                html_SeriesOverview += html_view_tab
                html_SeriesOverview += '<td>' + Status + '</td>'
                html_SeriesOverview += '</tr>'

                html_SeriesOverview += '<tr>'
                html_SeriesOverview += '<th ' + html_style_text_left + '>' + 'Finished:</th>'
                html_SeriesOverview += html_view_tab
                html_SeriesOverview += '<td>' + Beendet_Eingestellt + '</td>'
                html_SeriesOverview += '</tr>'

                html_SeriesOverview += '<tr>'
                html_SeriesOverview += '<th ' + html_style_text_left + '>' + 'Next Episode:</th>'
                html_SeriesOverview += html_view_tab
                html_SeriesOverview += '<td>' + NextEpisode + '</td>'
                html_SeriesOverview += '</tr>'

                html_SeriesOverview += '</table>' 
                html_SeriesOverview += '</tbody>'
                html_SeriesOverview += '<p></p>'

                
                
                if int(Staffel_veroeff) > 10:
                    html_SeriesOverview += html_table_style #staffel overview
                else:
                    html_SeriesOverview += '<table>'
                html_SeriesOverview += '<tbody>' 

                for Staffel_idx in range(int(Staffel_veroeff)):
                    
                    Staffel_key = "Staffel " + str(Staffel_idx+1)
                    Staffel_key_eng = "Season " + str(Staffel_idx+1)

                    Folgen_gesehen = mySeriesDict[key][Staffel_key][0]
                    Folgen_ges = mySeriesDict[key][Staffel_key][1]

                    if Staffel_idx < 10:
                        
                        html_SeriesOverview += '<tr>'
                        html_SeriesOverview += '<th ' + html_style_text_left + '>' + Staffel_key_eng + ':</th>'
                        html_SeriesOverview += html_view_tab
                        html_SeriesOverview += '<td>' + Folgen_gesehen + '</td>'
                        html_SeriesOverview += '<td>:</td>'
                        html_SeriesOverview += '<td>' + Folgen_ges + '</td>'
                        html_SeriesOverview += '</tr>'

                        if Staffel_idx == 9:
                            html_SeriesOverview += '</table>' 
                            html_SeriesOverview += '</tbody>'

                    elif Staffel_idx >= 10 and Staffel_idx < 20:
                        if Staffel_idx == 10:
                            html_SeriesOverview += '<table>' 
                            html_SeriesOverview += '<tbody>'
                        html_SeriesOverview += '<tr>'
                        html_SeriesOverview += '<th ' + html_style_text_left + '>' + Staffel_key_eng + ':</th>'
                        html_SeriesOverview += html_view_tab
                        html_SeriesOverview += '<td>' + Folgen_gesehen + '</td>'
                        html_SeriesOverview += '<td>:</td>'
                        html_SeriesOverview += '<td>' + Folgen_ges + '</td>'
                        html_SeriesOverview += '</tr>'
                if Staffel_idx >=10 and Staffel_idx < 20:
                    idx_leftover = 19-Staffel_idx
                    for idx in range(idx_leftover):
                        html_SeriesOverview += '<tr>'
                        html_SeriesOverview += '<th>&nbsp;</th>'
                        html_SeriesOverview += '</tr>'

                html_SeriesOverview += '</table>' 
                html_SeriesOverview += '</tbody>'
                html_SeriesOverview += '<hr /><hr />'    
        except:
            traceback.print_exc()




        html_overview_table += '</tbody>'
        html_overview_table += '</table>'
        html_overview_table += '<p></p>'
        html_overview_table += '<hr /><hr />'
  
            
        print "html overviewTable"   
        #print html_overview_table
        print "html Series Overview"
        #print html_SeriesOverview
        

        myHTMLfile.write(html_overview_table)
        myHTMLfile.write(html_SeriesOverview)

        print "HTML file created"
        
        
        


        myHTMLfile.close()
    
            

   



    


