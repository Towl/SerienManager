'''
Created on 10.01.2018

@author: Thorsten
'''

from cx_Freeze import setup, Executable

import sys 
import os



productName = "SerienStatistik"
if sys.platform == 'win32':
    base = "Win32GUI"

# def include_OpenGL():
#     path_base = "C:\\pyzo2013c\\Lib\\site-packages\\OpenGL"
#     skip_count = len(path_base) 
#     zip_includes = [(path_base, "OpenGL")]
#     for root, sub_folders, files in os.walk(path_base):
#         for file_in_root in files:
#             zip_includes.append(
#                     ("{}".format(os.path.join(root, file_in_root)),
#                      "{}".format(os.path.join("OpenGL", root[skip_count+1:], file_in_root))
#                     ) 
#             )      
#     return zip_includes






# "platforms\\" -> qwindows.dll needed to get the gui started
# "qt.conf"                     needed to display images
# ("imageformats\\","imageformats\\")] needed to display images



files = [("SeriesDict.txt","SeriesDict.txt"),
		("Serien_gui_qt.ui", "Serien_gui_qt.ui"),
		("bilder\\","bilder\\"),
		("platforms\\","platforms\\"),
		("imageformats\\","imageformats\\")]
		#("qt.conf","qt.conf")]

includes = ['PyQt5.QtCore','idna.idnadata']


options = {"build_exe": {
        "include_files": files ,
        "includes": includes,
        "packages": ["requests"]}
        }



executables = [Executable(
      script="main.py",
      base=base,
      targetName="SerienStatistik.exe"
     )]

setup(
      name="Serienstatistik",
      version="0.3.0",
      author="TWeigel",
      description="Copyright 2018",
      executables=executables,
      options = options

      ) 


