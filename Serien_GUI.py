'''
Created on 26.12.2017

@author: Thorsten
'''
# coding: utf8
import sys
import os
import datetime
import time
from data_handler import txt_Reader
from html_handler import html_Handler
 
from PyQt5 import QtCore, QtGui, QtWidgets, uic, Qt
from PyQt5.Qt import QStringListModel
from PyQt5.QtWidgets import QFileDialog, QApplication, QMessageBox
from click.exceptions import FileError






mytxt_Reader = txt_Reader()
myhtmlHandler = html_Handler()



class MySerienGUI(QtWidgets.QMainWindow):
    '''
    classdocs
    '''


    def __init__(self, parent=None):
        '''
        Constructor
        '''
        #super()
        QtWidgets.QMainWindow.__init__(self,parent)
        self.ui = uic.loadUi("Serien_gui_qt.ui",self)
        
        self.myDict = {}
        
        self.MaxRangeStaffeln = 30
        
        for i in range(self.MaxRangeStaffeln):
            mystr1 = "S"+str(i+1) + "_gesehen_spb"
            mystr2 = "S"+str(i+1) + "_ges_spb"
            
            child = self.ui.findChildren(QtWidgets.QSpinBox, mystr1)[0]
            #child.setEnabled(False)
            child.blockSignals(True)
            child = self.ui.findChildren(QtWidgets.QSpinBox, mystr2)[0]
            #child.setEnabled(False)
            child.blockSignals(True)

        # progressbar
        self.ui.progressBar.setValue(0)
        
        # Slots einrichten
        self.ui.OpenFile_bt.clicked.connect(self.onOpenFile)
        
        self.ui.Names_listWidget.itemSelectionChanged.connect(self.selectionChanged)
        
        self.ui.Save_File_bt.clicked.connect(self.onSaveFile)

        self.ui.activate_EditMode_checkbox.stateChanged.connect(self.EditModeActive_Changed)
        
        self.ui.Clean_bt.clicked.connect(self.onCleanFields)
    
        self.ui.Add_Serie_bt.clicked.connect(self.onAddSerie)
        
        self.ui.html_reader_bt.clicked.connect(self.onHtmlReader)
        
        self.ui.NewFile_bt.clicked.connect(self.onCreateNewFile)
        
        self.ui.remove_serie_bt.clicked.connect(self.onRemoveSerie)

        self.ui.change_serie_bt.clicked.connect(self.onChangeSerie)
        
        
        self.loadAllSeriesDict()
        
        
        # Initialize QCompleter for Series Entry
        self.AutoCompleter()
        
        
        
        
        
        self.ui.Version_label.setText("V0.3.0 by Towl_20181223")
        
        ''' letzte Optimierung
        - Backup der geoeffneten SerienListe wird beim Start erstellt 10.02.2018
        - Anpassung auf neue Serienjunkies Webseite 14.04.2018
        - Hinzufuegen des Attributes NextEpisode -> zeigt Ausstrahlungsdatum der Naechsten Episode an 18.04.2018
        - update html reader for new site 02.09.2018
        - add traceback funktion 02.09.2018
        - fix bug "beendet-Funktion" 02.09.2018
        - update new qt 23.12.2018
        '''  
        
        


        
        
    
##################################################################################
##################################################################################    
    
    def month_converter(self,month):
        months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        return months.index(month) + 1
    
    
    def loadAllSeriesDict(self):
        
        DateTime_today = datetime.datetime.now()
        #DateTime_today.hour
        #DateTime_today.mintue
        current_year = DateTime_today.year
        current_day = DateTime_today.day
        current_month = DateTime_today.month
        
        
        
        cwd = os.getcwd()
        file = cwd +"\\" + "seriesDict.txt"

        lastModified = time.ctime(os.path.getmtime(file))
        #print "last modified: {}".format(lastModified)
        lastModiefied_parts = str(lastModified).split(' ')
        file_year = lastModiefied_parts[4]
        file_month = lastModiefied_parts[1]
        file_day = lastModiefied_parts[2]
        
        file_month = self.month_converter(file_month)
        
        
      
        #update current AllSeriesDict if older than one month      
        if file_month < current_month:
            self.series_dict = myhtmlHandler.getAllSeries()
            self.allSeries = self.series_dict.keys()
            print "available series {}".format(len(self.allSeries))
            print "file_month < current_month {}<{}".format(file_month,current_month)
             
        elif file_year < current_year:
            self.series_dict = myhtmlHandler.getAllSeries()
            self.allSeries = self.series_dict.keys()
            print "available series {}".format(len(self.allSeries))
            print "file_year < current_year {}<{}".format(file_year,current_year)
        else:
            print "load available file"
            myfile = open(file,'r')
            self.series_dict = {}
        
            for line in myfile:
                new_line = line.rstrip() # remove whitespace and new_line sign
                new_line = new_line.split(";")
                self.series_dict[new_line[0]] = new_line[1]
                                        
            myfile.close()
            
            self.allSeries = self.series_dict.keys()
            print "available series {}".format(len(self.allSeries))
        
        

    
    
    
    def onChangeSerie(self):
        myDict = self.myDict
        myhtmlHandler.writeInformation(myDict)

    
    
    
    
    
    
    def onRemoveSerie(self):
        try:
            
            selected_item = str(self.ui.Names_listWidget.currentItem().text())
            
            QuestionRemoveSerie = QMessageBox.question(self,"Remove Serie?","Would you like to remove the selected Serie --{}--?".format(selected_item),
                                            QMessageBox.Yes|QMessageBox.No)
            if QuestionRemoveSerie == QMessageBox.Yes:
                print "YES {}".format(selected_item)
                self.ui.Names_listWidget.takeItem(self.ui.Names_listWidget.currentRow())
                self.CleanFields()
                self.myDict.pop(selected_item)
                
                # Load other available Item in List and display information
                key = str(self.Names_listWidget.currentItem().text())
                self.ui.Name_entry.setText(key)
                self.SetFields(key)  # includes fill_staffeln()          
                mytxt_Reader.showDict(self.myDict,key)
                self.SetImage(key)
                self.ui.remove_serie_bt.setEnabled(True)
                self.ui.change_serie_bt.setEnabled(True)
            else:
                print "Remove NO"
        except:
            pass
                
        
    
    def AutoCompleter(self):

        self.model = QStringListModel()
                
        mycompleter = QtWidgets.QCompleter()
        mycompleter.setCaseSensitivity(QtCore.Qt.CaseInsensitive)
        mycompleter.setFilterMode(QtCore.Qt.MatchContains)
        self.ui.Name_entry.setCompleter(mycompleter)
        
        
        
        mycompleter.setModel(self.model)
        self.model.setStringList(self.allSeries)
        
    
     
    def onCreateNewFile(self):
        
        QuestionCreateNewFile = QMessageBox.question(self, "New file" ,"Are you sure, would you like to create a new file?\nThe current one will not be saved!")
        if QuestionCreateNewFile == QMessageBox.Yes:
            print "Clean All for new File"
            self.CleanFields()
            self.myDict = {}
            self.ui.activate_EditMode_checkbox.setEnabled(True)
            self.ui.Names_listWidget.clear() # remove all items from the list widget
            
            
        else:
            QMessageBox.information(self, "No new List", "The list will not be cleared")
        
        
        
    
    
    
    def onImport(self):
        pass
        
                
    def CleanEpisods(self):
        for i in range(self.MaxRangeStaffeln):
            mystr1 = "S"+str(i+1) + "_gesehen_spb"
            mystr2 = "S"+str(i+1) + "_ges_spb"
            
            child = self.ui.findChildren(QtWidgets.QSpinBox, mystr1)[0]
            child.setValue(0)
            child.setStyleSheet("background-color: white")
            
            child = self.ui.findChildren(QtWidgets.QSpinBox, mystr2)[0]
            child.setValue(0)
    
    def CleanFields(self):
        self.ui.Name_entry.setText('')
        self.ui.Sprache_entry.setText('')
        self.ui.Staffel_veroeff_spb.setValue(1)
        self.ui.Staffel_angek_spb.setValue(1)
        self.ui.beendet_label.setText('')
        self.ui.Image_label.clear()
        self.ui.Image_label.setText('')
        self.ui.Next_Episode_label.setText('')

        self.CleanEpisods()
    
        
        
    
    def onCleanFields(self):
        
        self.CleanFields()
        
        self.ui.Add_Serie_bt.setEnabled(True)
        
        

            
        
    def GetFieldsSaveToDict(self):
        
        
        key = str(self.ui.Name_entry.text())
        Sprache = str(self.ui.Sprache_entry.text())
        Staffel_veroeff = self.ui.Staffel_veroeff_spb.value()
        Staffel_angek = self.ui.Staffel_angek_spb.value()
        Beendet_Eingestellt = str(self.ui.beendet_label.text())
        if Beendet_Eingestellt == '':
            Beendet_Eingestellt = 'false'
        else:
            Beendet_Eingestellt = 'true'
        
        temp_S_dict = {} 
        
        # Check if Serie is a list item
        item_in_list = self.ui.Names_listWidget.findItems(key, QtCore.Qt.MatchExactly)
        if not(item_in_list):
            QMessageBox.critical( self, "Information", "Serie does not exist in the List, please add the current Serie {} to the List".format(key))

        else:
            # Check if fields are filled
            if key != '' and Sprache != '' and Staffel_veroeff != 0 and Staffel_angek != 0:
                temp_S_dict[key] = {"Sprache":Sprache,
                                    "Staffeln_veroeff": str(Staffel_veroeff),
                                    "Staffeln_angek": str(Staffel_angek),
                                    "Beendet_Eingestellt": Beendet_Eingestellt
                                    }
            
                # Get all values for the available Staffeln
                for i in range(Staffel_veroeff):
                    mystr1 = "S"+str(i+1) + "_gesehen_spb"
                    mystr2 = "S"+str(i+1) + "_ges_spb"
                    myStaffel_str = "Staffel " + str(i+1) 
                    
                    child_gesehen = self.ui.findChildren(QtWidgets.QSpinBox, mystr1)[0]
                    gesehen = str(child_gesehen.value())
                    
                    child = self.ui.findChildren(QtWidgets.QSpinBox, mystr2)[0]
                    ges = str(child.value())
                    
                    if int(gesehen) < int(ges):
                        child_gesehen.setStyleSheet("background-color: rgb(244,139,62)")
                    else:
                        child_gesehen.setStyleSheet("background-color: white")
                    
                    temp_S_dict[key].update({myStaffel_str : [gesehen, ges]})
                
                # Update the Serie in myDict
                self.myDict.update(temp_S_dict)
                print "Current Serie was updated in myDict"
                mytxt_Reader.showDict(self.myDict,key)
                
            else:
                print "not all fields are filled"
        
        
        
        
    
    def onAddSerie(self):
        
        key = str(self.ui.Name_entry.text())
        Sprache = str(self.ui.Sprache_entry.text())
        Staffel_veroeff = self.ui.Staffel_veroeff_spb.value()
        Staffel_angek = self.ui.Staffel_angek_spb.value()
        Beendet_Eingestellt = str(self.ui.beendet_label.text())
              
            
        
        
        temp_S_dict = {} 
        
        if key != '' and Staffel_veroeff != 0 and Staffel_angek != 0:
            if Sprache == '':
                Sprache = 'DE'
                self.ui.Sprache_entry.setText('DE')
                
            temp_S_dict[key] = {"Sprache":Sprache,
                               "Staffeln_veroeff": str(Staffel_veroeff),
                               "Staffeln_angek": str(Staffel_angek),
                               "Beendet_Eingestellt": Beendet_Eingestellt
                                }
            
            
            for i in range(Staffel_veroeff):
                mystr1 = "S"+str(i+1) + "_gesehen_spb"
                mystr2 = "S"+str(i+1) + "_ges_spb"
                myStaffel_str = "Staffel " + str(i+1) 
                
                child = self.ui.findChildren(QtWidgets.QSpinBox, mystr1)[0]
                gesehen = str(child.value())
                
                
                
                child = self.ui.findChildren(QtWidgets.QSpinBox, mystr2)[0]
                ges = str(child.value())
            
                temp_S_dict[key].update({myStaffel_str : [gesehen, ges]})
            
            
            print temp_S_dict
            
            # Add Key (Series Name) to ListWidget 
            # Check if key does already exist      
            new_item = QtWidgets.QListWidgetItem(key)
            new_item_exist = self.ui.Names_listWidget.findItems(key, QtCore.Qt.MatchExactly)
            
            if not(new_item_exist):
                self.ui.Names_listWidget.addItem(new_item)
                self.myDict.update(temp_S_dict)
                print "Serie was updated in dict ***OnAddSerie***"
            else:
                QMessageBox.critical( self, "Information", "Serie does already exist,\n the new one will not be saved")

            
        else:
            print "please fill the entrys"
        
        
    
    def ActivateEntryButton(self):
        
        self.ui.Save_File_bt.setEnabled(True)        
        self.ui.activate_EditMode_checkbox.setEnabled(True)
        # html
        self.ui.html_reader_bt.setEnabled(True)
        
        
        
    def activate_deactivate_Fields(self,flag_status):    
        
        
        self.ui.Name_entry.setEnabled(flag_status)
        self.ui.Sprache_entry.setEnabled(flag_status)
        self.ui.Staffel_veroeff_spb.setEnabled(flag_status)
        self.ui.Staffel_angek_spb.setEnabled(flag_status)
        
        self.ui.Clean_bt.setEnabled(flag_status)
        
        if flag_status == False:
            self.ui.Add_Serie_bt.setEnabled(False)
        
        for i in range(self.MaxRangeStaffeln):
            mystr1 = "S"+str(i+1) + "_gesehen_spb"
            mystr2 = "S"+str(i+1) + "_ges_spb"
            
            child = self.ui.findChildren(QtWidgets.QSpinBox, mystr1)[0]
            child.setEnabled(flag_status)
            
            child = self.ui.findChildren(QtWidgets.QSpinBox, mystr2)[0]
            child.setEnabled(flag_status)
        
         
    def EditModeActive_Changed(self):
        
        if self.ui.activate_EditMode_checkbox.isChecked():
            
            
            
            # activate Fields
            self.activate_deactivate_Fields(True)

        else:
            
            # deactivate Fields
            self.activate_deactivate_Fields(False)

            self.GetFieldsSaveToDict()
            
            if self.myDict:
                self.ActivateEntryButton()
            
         
        
    
        
    
        
    def onSaveFile(self):
        
        

        list_items_count = range(self.ui.Names_listWidget.count())
        if len(list_items_count) > 0:
            QuestionSaveFile = QMessageBox.question(self,"Save which file","Would you like to save the current file? -> Press *YES*\nor\nSave as -> Press *NO*",
                                                    QMessageBox.Yes|QMessageBox.No|QMessageBox.Abort)

            try:
                try:
                    if QuestionSaveFile == QMessageBox.Yes:
                        the_file = os.path.isfile(self.openFileName_str)
                        if (the_file):
                            print "The file " + self.openFileName_str + " does exist!"
                            print "save file"
                            try:
                                mytxtReader = txt_Reader()
                                mytxtReader.saveMyFile(self.myDict, self.openFileName_str)
                                self.ui.OpenFileName_label.setText(self.openFileName_str)
                                QMessageBox.warning( self, "Information", "All Series saved!")
                            except IOError as e:
                                print "I/O error({0}): {1}".format(e.errno, e.strerror)
                                QMessageBox.warning( self, "Information", "Not saved, I/O error({0}): {1}".format(e.errno, e.strerror))
                            
                        else:       
                            QMessageBox.warning( self, "Information", "The file {} was not saved!".format(self.openFileName_str))
                            
                    elif QuestionSaveFile == QMessageBox.No:
                        SaveFileName_str, _ = QFileDialog.getSaveFileName(self, 'Save Serien Statistik', '*.txt', 'Text file', '*.txt')
                        endWithTXT = SaveFileName_str.endswith('.txt')
                        
                        if endWithTXT and len(SaveFileName_str) == 4:
                            QMessageBox.information( self, "Information", "The Series will not be saved, invalid filename -> {}".format(SaveFileName_str))
                        elif endWithTXT == False:
                            SaveFileName_str += '.txt'
                        else:
                               
                            print SaveFileName_str
                            try:
                                mytxtReader = txt_Reader()
                                mytxtReader.saveMyFile(self.myDict, SaveFileName_str)
                                self.ui.OpenFileName_label.setText(SaveFileName_str)
                                QMessageBox.warning( self, "Information", "All Series saved to /n{}!".format(SaveFileName_str))
                            except IOError as e:
                                print "I/O error({0}): {1}".format(e.errno, e.strerror)
                                QMessageBox.warning( self, "Information", "Not saved, I/O error({0}): {1}".format(e.errno, e.strerror))
                            
                    elif QuestionSaveFile == QMessageBox.Abort:
                        QMessageBox.information( self, "Information", "The Series will not be saved") 
                        
                        
                        
                # wird es jemals wieder aufgerufen?               
                except:
                    if QuestionSaveFile == QMessageBox.No:
                        SaveFileName_str, _ = QFileDialog.getSaveFileName(self, 'Save Serien Statistik', '*.txt', 'Text file', '*.txt')
                        endWithTXT = SaveFileName_str.endswith('.txt')
                        if endWithTXT == False:
                            SaveFileName_str += '.txt'
                            
                            
                        print SaveFileName_str
                        try:
                            mytxtReader = txt_Reader()
                            mytxtReader.saveMyFile(self.myDict, SaveFileName_str)
                            self.ui.OpenFileName_label.setText(SaveFileName_str)
                            QMessageBox.warning( self, "Information", "All Series saved to /n{}!".format(SaveFileName_str))
                        except IOError as e:
                            print "I/O error({0}): {1}".format(e.errno, e.strerror)
                            QMessageBox.warning( self, "Information", "Not saved, I/O error({0}): {1}".format(e.errno, e.strerror))
                            
                    elif QuestionSaveFile == QMessageBox.Abort:
                        QMessageBox.Information( self, "Information", "The Series will not be saved")
                        
            except FileError:
                    QMessageBox.warning( self, "Information", "Not saved")
            
                
    def selectionChanged(self):
        '''
        Function for SelectionChanged creates the view for all existing series names
        Input: self        
        '''
        
        try:
            key = str(self.Names_listWidget.currentItem().text())
            
            self.ui.Name_entry.setText(key)
            
            self.SetFields(key)  # includes fill_staffeln()          
            
            self.updateListStatus(key)
            

            mytxt_Reader.showDict(self.myDict,key)
            
            self.SetImage(key)
            
            self.ui.remove_serie_bt.setEnabled(True)
            self.ui.change_serie_bt.setEnabled(True)
            

        except:
            self.CleanFields()
            
            print "except selection changed"
    
    def updateListStatus(self,key):
        # Aktualisiere die Farbe der ListwidgetItems auf Basis, ob alle Folgen/Staffeln angeschaut wurden 
        for key in self.myDict.keys():
            AlleGesehen = 0 # Get Status fuer ListWidgetFarbe
            finishedListItem = self.ui.Names_listWidget.findItems(key, QtCore.Qt.MatchExactly)[0]          
            Staffel_veroeff = int(self.myDict[key]["Staffeln_veroeff"])  
            
            for Staffel_idx in range(Staffel_veroeff):
                Staffel_key = "Staffel " + str(Staffel_idx+1)
                
                Folgen_gesehen = int(self.myDict[key][Staffel_key][0])
                Folgen_ges = int(self.myDict[key][Staffel_key][1])

                
                if Folgen_gesehen == Folgen_ges:
                    AlleGesehen += 1
                        
                if AlleGesehen == Staffel_veroeff:
                
                    finishedListItem.setBackground(QtGui.QColor('#72da75'))
                else:                    
                    finishedListItem.setBackground(QtGui.QColor('white'))  
        
    def fill_Staffeln(self,key):
        
        Staffel_veroeff = int(self.myDict[key]["Staffeln_veroeff"])  
        Staffel_angek = int(self.myDict[key]["Staffeln_angek"])
        spinboxes = self.ui.findChildren(QtWidgets.QSpinBox)
        
        
        
        
        for Staffel_idx in range(Staffel_veroeff):
            Staffel_key = "Staffel " + str(Staffel_idx+1)
            Staffel_ges_str = "S" + str(Staffel_idx+1) + "_ges_spb" 
            Staffel_gesehen_str = "S" + str(Staffel_idx+1) + "_gesehen_spb"
            
            Folgen_gesehen = int(self.myDict[key][Staffel_key][0])
            Folgen_ges = int(self.myDict[key][Staffel_key][1])
            
            


                
            for spb in spinboxes:
                spb_objectName = str(spb.objectName())
                number_objectName = spb_objectName.split('_')
                try:
                    number_objectName = int(number_objectName[0][1:])
                except:
                    pass
                # Fill value Folgen gesamt je Staffel
                if spb_objectName == Staffel_ges_str:
                    try:
                        child = self.ui.findChildren(QtWidgets.QSpinBox, spb_objectName)[0]
                        child.setValue(Folgen_ges)
                        child.setStyleSheet("background-color: white")
    
    
                    except IndexError:
                        pass
                # Fill value Folgen gesehen je Staffel
                elif spb_objectName == Staffel_gesehen_str:
                    try:
                        child = self.ui.findChildren(QtWidgets.QSpinBox, spb_objectName)[0]
                        child.setValue(Folgen_gesehen)
                        child.setMaximum(Folgen_ges)
                        
                        # Set another time the new value because of bug
                        child.setValue(Folgen_gesehen)

                        
                        if Folgen_gesehen < Folgen_ges:
                            child.setStyleSheet("background-color: rgb(244,139,62)")
                        else:
                            child.setStyleSheet("background-color: white")
                    except IndexError:
                        pass
                # Fill 0 for all other spinboxes je Staffel
                elif not (spb_objectName == "Staffel_veroeff_spb" or spb_objectName == "Staffel_angek_spb") and not (number_objectName <= Staffel_veroeff):
                    try:
                        child = self.ui.findChildren(QtWidgets.QSpinBox, spb_objectName)[0]
                        child.setValue(0)
                        child.setStyleSheet("background-color: white")

                    except IndexError:
                        pass
                
                
                else:
                    pass
                    #print "else"
                
            
                

         
        
    def onOpenFile(self):
        list_items_count = range(self.ui.Names_listWidget.count())
        if len(list_items_count) > 0:
            QuestionOpenFile = QMessageBox.question(self, "New file" ,"Are you sure, would you like to open a new file?\nThe current one will not be saved!")
            if QuestionOpenFile == QMessageBox.Yes:
                openFile = 1
                print "Clean All for new File"
                self.CleanFields()
                self.myDict = {}
                self.ui.activate_EditMode_checkbox.setEnabled(True)
                self.ui.Names_listWidget.clear() # remove all items from the list widget
                
            else:
                openFile = 0
        else:
            openFile = 1
        
        if openFile:
            self.openFileName_str, _ = QFileDialog.getOpenFileName(self, "Bitte SerienStatistik.txt oeffnen", "Text file","*.txt")
            
            if self.openFileName_str:
                self.ui.OpenFileName_label.setText(self.openFileName_str)
                #print openFileName_str
                self.openFileName_str = str(self.openFileName_str)
                mytxtReader = txt_Reader()
                myDict = mytxtReader.openMyFile(self.openFileName_str)
                self.myDict = myDict.myDict
                
                if self.myDict:
                    # Fill List with myDict keys
                    for key in self.myDict.keys():
                        item = QtWidgets.QListWidgetItem(key)
                        self.ui.Names_listWidget.addItem(item)
                    
                    # Select First Item in ListWidget 
                    self.ui.Names_listWidget.setCurrentRow(0)
                    
                    # Set Buttons/Entrys/Checkbox Enabled    
                    self.ActivateEntryButton() 
                else:
                    QMessageBox.warning( self, "Information", "The current file *{}* is empty.".format(self.openFileName_str))
                    
    def updateSeries(self,key):
        if key != '':
                
            try:        
                Serie_Dict = myhtmlHandler.getInformation(key, self.series_dict)  
                
                
                Staffel_veroeff = Serie_Dict[key]["Staffeln_veroeff"]
                Staffel_angek = Serie_Dict[key]["Staffeln_angek"]
                Beendet_Eingestellt = Serie_Dict[key]["Beendet_Eingestellt"]
                IMG = Serie_Dict[key]["IMG"]
                Next_Episode = Serie_Dict[key]["Next_Episode"] 
                
                
                
                self.myDict[key].update({"Staffeln_veroeff": str(Staffel_veroeff),
                                        "Staffeln_angek": str(Staffel_angek),
                                        "Beendet_Eingestellt": str(Beendet_Eingestellt),
                                        "IMG": str(IMG),
                                        "Next_Episode": str(Next_Episode)})
                
                for S in range(int(Staffel_veroeff)):
                    myS_str = "Staffel " + str(S+1)
                    try:
                        gesehen = self.myDict[key][myS_str][0]
                        ges_alt = self.myDict[key][myS_str][1]
                    except:
                        gesehen = str(0)
                        ges_alt = str(0)

                    
                    
                    ges_new = Serie_Dict[key][myS_str]
                    #print "gesamt alt/neu {}/{}".format(ges_alt,ges_new)
                    self.myDict[key].update({myS_str:[gesehen,ges_new]})
                        
                self.SetFields(key)
                self.SetImage(key)
            except Exception as ex:
                QMessageBox.warning( self, "Information", "Exception, {}".format(ex))
            except: # wird auch aufgerufen wenn irgendein Lesefehler im HTML Reader auftritt
                QMessageBox.warning( self, "Information", "The Serie {} does not exist, please check for correct writing.".format(key))
                self.ui.beendet_label.setText('')
                self.ui.Image_label.clear()
                self.ui.Image_label.setText('')

                self.CleanEpisods()    
                
        else:
            QMessageBox.warning( self, "Information", "There is no Name for the Serie")            
        
    def onHtmlReader(self):
        key = str(self.ui.Name_entry.text()) 
        QuestionUpdateAll = QMessageBox.question(self,"Update all Series","Would you like to update all Series (Yes to All)?\n" \
                                                 "or only the selected one (Yes) -> {} ?".format(key),
                                                QMessageBox.YesAll|QMessageBox.Yes|QMessageBox.Abort)
        
        if QuestionUpdateAll == QMessageBox.Yes:
            self.updateSeries(key)
            print key
            print "-- updated --"
            
        elif QuestionUpdateAll == QMessageBox.YesAll:    
            list_items = range(self.ui.Names_listWidget.count())
            length = len(list_items)
            progress_completed = 0
            self.ui.progressBar.setValue(progress_completed)

              
            for pos in list_items:
                try:
                    item = ""
                    item = str(self.ui.Names_listWidget.item(pos).text())
                    print item
                    time.sleep(5)
                    #print "-"
                    print item
                    self.updateSeries(item)
                    print "done"
                    time.sleep(0.25)
                    progress_completed += 100./length
                    
                    self.ui.progressBar.setValue(progress_completed)
                    print "Progress {} / 100 %".format(round(progress_completed,2))
                    print "Serie {} / {}".format(pos+1,length)
                    print ""

                except IOError as IOerr:
                    print ""
                    print "IOerr"
                    print IOerr[0]
                    print IOerr[1]
                
                except Exception as ex:
                    print item
                    print ex
                
            
            # Select First Item in ListWidget 
            self.ui.Names_listWidget.setCurrentRow(0)
            
            # call manual selectionChanged to update the relevant fields
            self.selectionChanged()
            
            
            
        elif QuestionUpdateAll == QMessageBox.Abort:
            pass
        
    def SetImage(self,key):
        try:
            # Get Images of current working directory
            bilder = [x for x in os.listdir('./bilder//') if x.endswith(".jpg")]
            cwd = os.getcwd()
            try:
                image_name = self.myDict[key]["IMG"]
                #print image_name
            except:
                print "no img in mydict"
            
            new_path = cwd + '\\bilder\\' + image_name
                     
            if image_name in bilder:
                #print image_name
                
                image = QtGui.QPixmap(new_path)
                
    
                self.ui.Image_label.setPixmap(image)
                self.ui.Image_label.setText("")
            else:
                self.ui.Image_label.clear()
                self.ui.Image_label.setText("no Image available")
        except:
            self.ui.Image_label.setText("no Image available")    
        
        
    def SetFields(self,key):
        self.ui.Sprache_entry.setText(self.myDict[key]["Sprache"])
        self.ui.Staffel_veroeff_spb.setValue(int(self.myDict[key]["Staffeln_veroeff"]))
        self.ui.Staffel_veroeff_spb.setStyleSheet("background-color: white")
        self.ui.Staffel_angek_spb.setValue(int(self.myDict[key]["Staffeln_angek"]))
        self.ui.Staffel_angek_spb.setStyleSheet("background-color: white")
        
        try:
            if self.myDict[key]["Beendet_Eingestellt"] == 'true':
                self.ui.Next_Episode_label.setText("")
            else:
                self.ui.Next_Episode_label.setText(self.myDict[key]["Next_Episode"])
        except:
            self.ui.Next_Episode_label.setText("")    
        
        
        if self.myDict[key]["Beendet_Eingestellt"] == 'true':
            self.ui.beendet_label.setText("Beendet/Eingestellt")
        else:
            self.ui.beendet_label.setText("")
        
        
        self.fill_Staffeln(key)    
        
                
        
        
        