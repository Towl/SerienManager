'''
Created on 26.12.2017

@author: Thorsten
'''
# coding: utf8

from shutil import copyfile
import os

class txt_Reader(object):
    '''
    classdocs
    '''


    def __init__(self):
        '''
        Constructor
        '''
        
                      
    def openMyFile(self,FileLocation):
        
       
        backup_FileLocation = FileLocation
        try:
            pos = backup_FileLocation.find(".txt")
            backup_FileLocation = backup_FileLocation[:pos] + "_backup.txt"
        except:
            pass
            
            
        self.myDict = {}
        myfile = open(FileLocation,'r')
        print "file wurde geoeffnet"
        
        if os.stat(FileLocation).st_size != 0:
            for line in myfile:
                new_line = line.rstrip()
                new_line = new_line.split(";")
                self.myDict[new_line[0]] = {"Sprache":new_line[1],
                                       "Staffeln_veroeff": new_line[2],
                                       "Staffeln_angek": new_line[3],
                                       "Beendet_Eingestellt": new_line[4],
                                       "IMG": new_line[5]
                                       }
                # Pruefen ob Next_Episode in Struktur schon vorhanden
                if new_line[6].isdigit():     
                    for idx in range(int(new_line[2])):
                        self.myDict[new_line[0]].update({"Staffel " + str(idx+1) : [new_line[2*idx + 6], new_line[2*idx + 7]]})
                else:
                    self.myDict[new_line[0]].update({"Next_Episode": new_line[6]})
                    
                    for idx in range(int(new_line[2])):
                        self.myDict[new_line[0]].update({"Staffel " + str(idx+1) : [new_line[2*idx + 7], new_line[2*idx + 8]]})
                #print self.myDict
            
            copyfile(FileLocation, backup_FileLocation)
            print "backup created"
        else:
            print "file is empty"
                                
        myfile.close()
        #print self.myDict
        print "File wurde geschlossen"
        
        return self
    
    
        
            
    def showDict(self, myDict, key=''):
        print "----------------------------------------------------------------"
        print "----------------------------------------------------------------"
        print "SHOW MY DICT"
        print "----------------------------------------------------------------"
        print "----------------------------------------------------------------"
        
        if key != '':
            self.printDict(myDict, key)
            
        else:
            for key in myDict.keys():
                self.printDict(myDict, key)
                
                
        print "----------------------------------------------------------------"
        print "----------------------------------------------------------------"  


    
    def printDict(self, myDict, key):
        print "Name: {}".format(key)
        print "Sprache: {}".format(myDict[key]["Sprache"])
        print "Staffeln_veroeff: {}".format(myDict[key]["Staffeln_veroeff"])
        print "Staffeln_angek: {}".format(myDict[key]["Staffeln_angek"])
        print "Beendet_Eingestellt: {}".format(myDict[key]["Beendet_Eingestellt"])
        try:
            print "IMG: {}".format(myDict[key]["IMG"])
        except:
            print "IMG: no image"
            
        try:
            print "Next_Episode: {}".format(myDict[key]["Next_Episode"])
        except:
            print "Next_Episode: not available"
            
        for i in range(int(myDict[key]["Staffeln_veroeff"])):
            myStaffel_str = "Staffel " + str(i+1) 
            print "{}: {}".format(myStaffel_str,myDict[key][myStaffel_str])
                
              
    def saveMyFile(self, myDict, FileLocation):
        print "Save all Series to the txt-File"
        self.myDict = myDict
        myfile = open(FileLocation,'w+') # If not exist create new file (w+)
        try:
            for key in self.myDict.keys():
                message_key = "{}".format(key)
                for key2 in self.myDict[key].keys():
                    if key2 == "Sprache":
                        message_Sprache = "{}".format(self.myDict[key][key2])
                    elif key2 == "Staffeln_veroeff":
                        message_S_veroeff = "{}".format(self.myDict[key][key2])
                    elif key2 == "Staffeln_angek":
                        message_S_angek = "{}".format(self.myDict[key][key2])
                    elif key2 == "Beendet_Eingestellt":
                        message_Beendet_Eingestellt = "{}".format(self.myDict[key][key2])  
                    elif key2 == "IMG":
                        message_IMG = "{}".format(self.myDict[key][key2])
                    elif key2 == "Next_Episode":
                        message_Next_Episode = "{}".format(self.myDict[key][key2])  
                    
                for Staffel_idx in range(int(self.myDict[key]["Staffeln_veroeff"])):
                    Staffel_key = "Staffel " + str(Staffel_idx+1)
                    if Staffel_idx == 0:
                        message_S = ";"
                    else:
                        message_S += ";"
                    message_S += "{};{}".format(self.myDict[key][Staffel_key][0], self.myDict[key][Staffel_key][1])
                                    
                message = message_key + ";" + message_Sprache + ";" + message_S_veroeff + ";" + message_S_angek + \
                          ";" + message_Beendet_Eingestellt + ";" + message_IMG + ";" + message_Next_Episode + message_S + "\n"
                #print message
                myfile.write(message)
            
            message_S = []
            message = []
            print "Schreibvorgang abgeschlossen"
            myfile.close()
            print "File geschlossen"
        except:
            print "Schreibvorgang nicht moeglich"
            myfile.close()
            print "File geschlossen"
            
            

            
            